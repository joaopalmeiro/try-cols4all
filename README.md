# try-cols4all

## Development

Install [RStudio](https://posit.co/download/rstudio-desktop/)

```bash
rig add --pak-version stable 4.3.0
```

```bash
rig list
```

```bash
rig rstudio 4.3-arm64
```

## References

- https://github.com/mtennekes/cols4all
- https://cran.r-project.org/web/packages/cols4all/index.html
- https://github.com/r-lib/pak + https://pak.r-lib.org/
- https://pak.r-lib.org/dev/reference/pak_package_sources.html#package-source-details
- https://github.com/r-lib/styler/ + https://cran.r-project.org/web/packages/styler/index.html
- https://github.com/d3/d3-scale-chromatic
- https://github.com/Nowosad/colorblindcheck

## Notes

- https://github.com/e-/TreeColors.js/
- https://github.com/r-lib/rig
  - `rig add --help`
  - `rig rstudio --help`
  - `rig system add-pak --help`
  - `rig rm 4.3-arm64`
- https://cran.radicaldevelop.com/
